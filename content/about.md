+++
title = "À propos"
description = "Le Réseau Françaus des Centres Spatiaux Universitaires"
date = "2020-01-21"
aliases = ["about-us","about-hugo","contact"]
author = "Antoine Tavant"
+++

Le Réseau Français des Centres Spatiaux Universitaires et Étudiants (RFCSU) est une structure qui promeut la collaboration entres les differents acteurs des CSU.


Pour plus d'information, veuillez contacter antoine.tavant at lpp.polytechnique.fr


