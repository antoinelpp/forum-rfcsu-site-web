+++
author = "Antoine Tavant"
title = "Planning du forum 2020"
date = "2020-01-21"
description = "Le planning du Forum 2020 "
tags = [
    "Forum2020",
]
categories = [
    "Forum",
    "2020",
]
series = ["Themes Guide"]
aliases = ["migrate-from-jekyl"]
featured_image = "images/wall-clock-at-5-50-707582.jpg"

+++

Le planning préliminaire du Forum 2020.
Cette version n'est pas définitive, mais donne un bon aperçu de ce qui est prévu.

En résumé, le forum est consitué d'une alternance de présentation plénière et de workshops thématiques en parallèles.

<!--more-->

|           | **Jeudi 26 Mars**                      |                  |
| --------- | -------------------------------------- | ---------------- |
| **Heure** | **Activité**                           | **Lieu**         |
| 8h30      | Accueil café                           | couloir          |
| 9h30      | Discourt d'introduction                | Amphi Gay-Lussac |
| 9h40      | Keynote invité 1 : Nom Prénom fonction | Amphi Gay-Lussac |
| 10h       | Keynote invité 2 : Nom Prénom fonction | Amphi Gay-Lussac |
| 10h20     | Pause     café                         | Couloir          |
| 11h       | Session plénière n°1                     | Amphi Gay-Lussac |
| 12h15     | Flash poster pitchs                    | Amphi Gay-Lussac |


<table class="lunch_style"> 
<tr>
  <td> 12h30</td>
  <td> Session Poster et Repas </td> 
  <td> Couloir </td>
</tr>
 </table>

|       |                                         |                  |
| ----- | --------------------------------------- | ---------------- |
| 14h00 | Workshops techniques en parallèle n°1    | Petites Classes  |
| 15h30 | Pause café                              | Couloir          |
| 16h00 | Restitution des workshops et discussion | Amphi Gay-Lussac |
| 18h00 | After works                             | Couloir          |



<br>


|           | **Vendredi 27 Mars**                   |                  |
| --------- | -------------------------------------- | ---------------- |
| **Heure** | **Activité**                           | **Lieu**         |
| 8h30      | Accueil café                           | couloir          |
| 9h30      | Keynote invité 3 : Nom Prénom fonction | Amphi Gay-Lussac |
| 10h00     | Session plénière n° 2                  | Amphi Gay-Lussac |
| 11h00     | Pause     café  et expo                | Couloir          |
| 11h30     | Workshops techniques en parallèle n°2   | Amphi Gay-Lussac |

<table class="lunch_style"> 
<tr>
  <td> 12h30</td>
  <td> Session Poster et Repas </td> 
  <td> Couloir </td>
</tr>
 </table>

|       |                                      |                  |
| ----- | ------------------------------------ | ---------------- |
| 14h00 | Restitution workshops techniques n°2 | Amphi Gay-Lussac |
| 15h30 | Pause café                           | Couloir          |
| 16h00 | Temps libre                          |                  | 
