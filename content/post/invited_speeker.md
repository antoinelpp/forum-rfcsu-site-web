+++
author = "Antoine Tavant"
title = "Invités du forum 2020"
date = "2020-01-31"
description = "Liste des invités au Forum du RFCSU 2020 "
tags = [
    "Forum2020",
]
categories = [
    "Forum",
    "2020",
]
series = ["Themes Guide"]
aliases = ["migrate-from-jekyl"]
featured_image = "images/people-sitting-on-gang-chairs-2774556.jpg"

+++

Invités à la conférences :

* **Philippe Montpert**, Managing Director à **Willis Inspace**, cousier d'assurence en aérospatial. Présentation prévue jeudi 26 dans la matinée.
* **Nicolas Verdier**, Chef de projet JANUS et EyeSat, **CNES**
* **Pierre Drossart**, Directeur de recherche au LESIA et membre de la prospective INSU pour le Defi n°12
* **Tous les Centres Spatiaux Universitaires et Étudiants**