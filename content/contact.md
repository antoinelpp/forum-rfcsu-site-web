---
title: Contact
omit_header_text: true
description: We'd love to hear from you
type: page
menu: main

---


Vous pouvez contacter Antoine Tavant par email à l'adresse antoine.tavant at lpp.polytechnique.fr


