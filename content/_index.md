+++
author = "Antoine Tavant"
title = "Réseau Français des CSU"
+++

# ANNULÉ Forum des CSU
Le **1er forum** des CSU et CSE initialement prévu les 26 et 27 mars 2019 est annulé.

Dès qu'une nouvelle date est fixée, elle sera transmise ici.

## Planning

Le planning prévisionel est [accessible ici.](post/planning_forum2020)